import express, { Application } from "express";
import mongoose from "mongoose";
import cors from "cors";
import { Config } from "./config";
import { connectToDb } from "./database/connectToDb";

const activitiesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  start_date: {
    type: String,
    required: true,
  },
  description: String,
  place: String,
  category: String,
});

const Activities = mongoose.model("Activities", activitiesSchema);

export const createApp = async (config: Config) => {
  const app: Application = express();

  await connectToDb(config);

  app.use(express.json());

  app.use(cors());

  app.get("/activities", async (request, response) => {
    try {
      const activities = await Activities.find().lean().exec();
      response.status(200).json({ data: activities });
    } catch (error) {
      console.error(error);
      response.status(400).end();
    }
  });

  return app;
};
