import mongoose from "mongoose";
import { Config } from "../config";

export async function connectToDb(config: Config) {
  if (config.databaseUrl) {
    try {
      await mongoose.connect(config.databaseUrl),
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        };
    } catch (error) {
      console.error(error);
    }
  } else {
    throw new Error("Database URL not provided");
  }
}
