require("dotenv").config();
import { config } from "./config";
import { createApp } from "./createApp";

createApp(config).then((server) => {
  server.listen(config.port, () => {
    console.log(`Server Running at https://localhost:${config.port}`);
  });
});
